package ru.andreymarkelov.atlas.plugins;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;

import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.plugin.workflow.AbstractWorkflowPluginFactory;
import com.atlassian.jira.plugin.workflow.WorkflowPluginValidatorFactory;
import com.opensymphony.workflow.loader.AbstractDescriptor;
import com.opensymphony.workflow.loader.ValidatorDescriptor;

public class RegexCfValidatorFactory extends AbstractWorkflowPluginFactory implements WorkflowPluginValidatorFactory {
    public static String CUSTOMFIELDS = "fields";
    public static String CUSTOMFIELD = "cfId";
    public static String CUSTOMFIELDVIEW = "cfView";
    public static String MSG = "msg";
    public static String REGEX = "regex";
    public static final String ISCONFIGURED = "isConfigured";

    private Map<String, String> getCustomFields() {
        Map<String, String> map = new TreeMap<String, String>();
        List<CustomField> fields = ComponentAccessor.getCustomFieldManager().getCustomFieldObjects();
        for (CustomField field : fields) {
            map.put(field.getId(), field.getName());
        }
        return map;
    }

    @Override
    public Map<String, ?> getDescriptorParams(Map<String, Object> conditionParams) {
        Map<String, Object> map = new HashMap<String, Object>();

        if (conditionParams != null && conditionParams.containsKey(CUSTOMFIELD) &&
                conditionParams.containsKey(REGEX) && conditionParams.containsKey(MSG)) {
            String cfId = extractSingleParam(conditionParams, CUSTOMFIELD);
            String regex = extractSingleParam(conditionParams, REGEX);
            String msg = extractSingleParam(conditionParams, MSG);

            if (Utils.isValidStr(cfId)) {
                map.put(CUSTOMFIELD, cfId);
            } else {
                map.put(CUSTOMFIELD, "");
            }

            if (Utils.isValidStr(regex)) {
                map.put(REGEX, regex);
            } else {
                map.put(REGEX, "");
            }

            if (Utils.isValidStr(msg)) {
                map.put(MSG, msg);
            } else {
                map.put(MSG, "");
            }
        } else {
            map.put(CUSTOMFIELD, "");
            map.put(REGEX, "");
            map.put(MSG, "");
        }

        return map;
    }

    private String getParam(AbstractDescriptor descriptor, String param) {
        if (!(descriptor instanceof ValidatorDescriptor)) {
            throw new IllegalArgumentException("Descriptor must be a ValidatorDescriptor.");
        }

        ValidatorDescriptor validatorDescriptor = (ValidatorDescriptor) descriptor;
        String value = (String) validatorDescriptor.getArgs().get(param);
        if (value!=null && value.trim().length() > 0) {
            return value;
        } else  {
            return "";
        }
    }

    @Override
    protected void getVelocityParamsForEdit(Map<String, Object> velocityParams, AbstractDescriptor descriptor) {
        velocityParams.put(CUSTOMFIELDS, getCustomFields());
        velocityParams.put(CUSTOMFIELD, getParam(descriptor, CUSTOMFIELD));
        velocityParams.put(REGEX, getParam(descriptor, REGEX));
        velocityParams.put(MSG, getParam(descriptor, MSG));
    }

    @Override
    protected void getVelocityParamsForInput(Map<String, Object> velocityParams) {
        velocityParams.put(CUSTOMFIELDS, getCustomFields());
        velocityParams.put(CUSTOMFIELD, "");
        velocityParams.put(REGEX, "");
        velocityParams.put(MSG, "");
    }

    @Override
    protected void getVelocityParamsForView(Map<String, Object> velocityParams, AbstractDescriptor descriptor) {
        String cfId = getParam(descriptor, CUSTOMFIELD);
        String regex = getParam(descriptor, REGEX);
        String msg = getParam(descriptor, MSG);
        Boolean isConfigured = Boolean.TRUE;

        String cfView;
        CustomField field = ComponentAccessor.getCustomFieldManager().getCustomFieldObject(cfId);
        if (field != null) {
            cfView = field.getName();
        } else {
            cfView = ComponentAccessor.getJiraAuthenticationContext().getI18nHelper().getText("ru.andreymarkelov.atlas.plugins.utils.regexvalidator.customfieldoptinvalid");
            isConfigured = Boolean.FALSE;
        }

        if (StringUtils.isEmpty(msg)) {
            msg = ComponentAccessor.getJiraAuthenticationContext().getI18nHelper().getText("ru.andreymarkelov.atlas.plugins.utils.regexvalidator.regexerror", regex);
        }

        try {
            Pattern.compile(regex);
        } catch (Exception e) {
            regex = ComponentAccessor.getJiraAuthenticationContext().getI18nHelper().getText("ru.andreymarkelov.atlas.plugins.utils.regexvalidator.regexoptinvalid");
            isConfigured = Boolean.FALSE;
        }

        velocityParams.put(ISCONFIGURED, isConfigured);
        velocityParams.put(CUSTOMFIELD, cfId);
        velocityParams.put(CUSTOMFIELDVIEW, cfView);
        velocityParams.put(REGEX, regex);
        velocityParams.put(MSG, msg);
    }
}
