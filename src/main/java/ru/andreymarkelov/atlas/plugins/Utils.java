package ru.andreymarkelov.atlas.plugins;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.ComponentManager;
import com.atlassian.jira.bc.issue.search.SearchService;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.search.SearchException;
import com.atlassian.jira.issue.search.SearchResults;
import com.atlassian.jira.web.bean.PagerFilter;
import com.atlassian.query.Query;

public class Utils {
    private static final Logger log = LoggerFactory.getLogger(Utils.class);

    public static void closeConnection(Connection conn) {
        if (conn != null) {
            try {
                conn.close();
            } catch (SQLException e) {
            }
        }
    }

    public static void closeResultSet(ResultSet rs) {
        if (rs != null) {
            try {
                rs.close();
            } catch (SQLException e) {
            }
        }
    }

    public static void closeStaement(Statement stmt) {
        if (stmt != null) {
            try {
                stmt.close();
            } catch (SQLException e) {
            }
        }
    }

    public static List<Issue> executeJQLQuery(String jqlQuery) {
        List<Issue> result = null;

        User user = ComponentManager.getInstance().getJiraAuthenticationContext().getLoggedInUser();
        SearchService searchService = ComponentManager.getComponentInstanceOfType(SearchService.class);
        SearchService.ParseResult parseResult = searchService.parseQuery(user, jqlQuery);

        if (parseResult.isValid()) {
            Query query = parseResult.getQuery();
            try {
                SearchResults results = searchService.search(user, query, PagerFilter.getUnlimitedFilter());
                result = results.getIssues();
            } catch (SearchException e) {
                log.error("Utils::search exception during executing JQL", e);
            }
        }

        return result;
    }

    public static boolean isPositiveInteger(String str) {
        try {
            Integer i = Integer.valueOf(str);
            return (i >= 0) ? true : false;
        } catch (NumberFormatException nex) {
            return false;
        }
    }

    public static boolean isValidStr(String str) {
        return (str != null && str.length() > 0);
    }

    public static <K, V extends Comparable<V>> Map<K, V> sortByValues(final Map<K, V> map) {
        Comparator<K> valueComparator =  new Comparator<K>() {
            public int compare(K k1, K k2) {
                return map.get(k1).compareTo(map.get(k2));
            }
        };
        Map<K, V> sortedByValues = new TreeMap<K, V>(valueComparator);
        sortedByValues.putAll(map);
        return sortedByValues;
    }

    private Utils() {}
}
