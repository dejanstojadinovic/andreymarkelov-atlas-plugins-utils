package ru.andreymarkelov.atlas.plugins;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.bc.issue.search.SearchService;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.search.SearchException;
import com.atlassian.jira.issue.search.SearchResults;
import com.atlassian.jira.jql.builder.JqlQueryBuilder;
import com.atlassian.jira.jql.parser.DefaultJqlQueryParser;
import com.atlassian.jira.jql.parser.JqlParseException;
import com.atlassian.jira.web.bean.PagerFilter;
import com.atlassian.query.Query;
import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.InvalidInputException;
import com.opensymphony.workflow.Validator;
import com.opensymphony.workflow.WorkflowException;

public class UniqueValidator implements Validator {
    private final CustomFieldManager customFieldManager;
    private final SearchService searchService;
    private SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

    public UniqueValidator(CustomFieldManager customFieldManager, SearchService searchService) {
        this.customFieldManager = customFieldManager;
        this.searchService = searchService;
    }

    private boolean checkValue(Issue current, Issue other, String prop) {
        if (prop.startsWith("customfield")) {
            CustomField cf = customFieldManager.getCustomFieldObject(prop);
            if (cf != null) {
                return current.getCustomFieldValue(cf).toString().compareTo(other.getCustomFieldValue(cf).toString()) == 0;
            }
        } else if (prop.equals("due")) {
            return toDate(current.getDueDate()).compareTo(toDate(other.getDueDate())) == 0;
        } else if (prop.equals("created")) {
            return toDate(current.getCreated()).compareTo(toDate(other.getCreated())) == 0;
        }
        return false;
    }

    private String toDate(Timestamp timestamp) {
        return sdf.format(new Date(timestamp.getTime())).toString();
    }

    @Override
    public void validate(Map transientVars, Map args, PropertySet ps) throws InvalidInputException, WorkflowException {
        Issue issue = (Issue) transientVars.get("issue");
        String cfId = (String) args.get(UniqueCfValidatorFactory.ISSUEPROP);
        String jql = (String) args.get(UniqueCfValidatorFactory.JQL);
        Object countObj = args.get(UniqueCfValidatorFactory.COUNT);
        Integer count = (countObj != null) ? Integer.valueOf(countObj.toString()) : 0;
        String error = (String) args.get(UniqueCfValidatorFactory.ERROR);

        String errorview;
        if (!Utils.isValidStr(error)) {
            error = "ru.andreymarkelov.atlas.plugins.utils.uniquevalidator.jqlunique.error";
            errorview = ComponentAccessor.getJiraAuthenticationContext().getI18nHelper().getText(error);
        } else {
            errorview = String.format(error);
        }

        try {
            User user = ComponentAccessor.getJiraAuthenticationContext().getLoggedInUser();
            Query query = (new DefaultJqlQueryParser()).parseQuery(jql);
            if (issue.getKey() != null) {
                JqlQueryBuilder builder = JqlQueryBuilder.newBuilder();
                builder.where().addClause(query.getWhereClause()).and().not().issue(issue.getKey());
                builder.orderBy().setSorts(query.getOrderByClause().getSearchSorts());
                query = builder.buildQuery();
            }
            SearchResults results = searchService.search(user, query, PagerFilter.getUnlimitedFilter());
            if (results != null) {
                int realCount = 0;
                for (Issue i : results.getIssues()) {
                    if (checkValue(issue, i, cfId)) {
                        realCount++;
                    }
                }
                if (realCount > count) {
                    throw new InvalidInputException(errorview);
                }
            }
        } catch (SearchException e) {
            throw new InvalidInputException("Internal error in UniqueValidator");
        } catch (JqlParseException e) {
            throw new InvalidInputException("Internal error in UniqueValidator");
        }
    }
}
