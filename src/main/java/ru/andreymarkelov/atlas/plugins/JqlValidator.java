package ru.andreymarkelov.atlas.plugins;

import java.util.Map;
import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.bc.issue.search.SearchService;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.search.SearchException;
import com.atlassian.jira.issue.search.SearchResults;
import com.atlassian.jira.jql.builder.JqlQueryBuilder;
import com.atlassian.jira.jql.parser.DefaultJqlQueryParser;
import com.atlassian.jira.jql.parser.JqlParseException;
import com.atlassian.jira.web.bean.PagerFilter;
import com.atlassian.query.Query;
import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.InvalidInputException;
import com.opensymphony.workflow.Validator;
import com.opensymphony.workflow.WorkflowException;

public class JqlValidator implements Validator {
    private final SearchService searchService;

    public JqlValidator(SearchService searchService) {
        this.searchService = searchService;
    }

    @Override
    public void validate(Map transientVars, Map args, PropertySet ps) throws InvalidInputException, WorkflowException {
        Issue issue = (Issue) transientVars.get("issue");
        String jql = (String) args.get(JqlValidatorFactory.JQL);
        Object countObj = args.get(JqlValidatorFactory.COUNT);
        Integer count = (countObj != null) ? Integer.valueOf(countObj.toString()) : 0;
        String error = (String) args.get(JqlValidatorFactory.ERROR);
        Object excludeItselfObj = args.get(JqlValidatorFactory.EXCLUDEITSELF);
        Boolean excludeItself = (excludeItselfObj != null) ? Boolean.valueOf(args.get(JqlValidatorFactory.EXCLUDEITSELF).toString()) : Boolean.TRUE;

        if (!Utils.isValidStr(jql)) {
            return;
        }

        User user = ComponentAccessor.getJiraAuthenticationContext().getLoggedInUser();
        if (user == null) {
            return;
        }

        try {
            Query query = (new DefaultJqlQueryParser()).parseQuery(jql);
            if (excludeItself && issue.getKey() != null) {
                JqlQueryBuilder builder = JqlQueryBuilder.newBuilder();
                builder.where().addClause(query.getWhereClause()).and().not().issue(issue.getKey());
                builder.orderBy().setSorts(query.getOrderByClause().getSearchSorts());
                query = builder.buildQuery();
            }
            SearchResults results = searchService.search(user, query, PagerFilter.getUnlimitedFilter());
            if (results != null && results.getIssues().size() > count) {
                Issue i = results.getIssues().get(0);
                String message;
                if (!Utils.isValidStr(error)) {
                    message = ComponentAccessor.getJiraAuthenticationContext().getI18nHelper().getText("ru.andreymarkelov.atlas.plugins.utils.jqlvalidator.jqlunique.error", i.getKey(), i.getSummary());
                } else {
                    message = String.format(error, i.getKey().concat(": ").concat(i.getSummary()));
                }
                throw new WorkflowException(message);
            }
        } catch (JqlParseException ex) {
            return;
        } catch (SearchException e) {
            return;
        }
    }
}
