package ru.andreymarkelov.atlas.plugins.amutils.conditions;

import java.util.HashMap;
import java.util.Map;

import com.atlassian.jira.plugin.workflow.AbstractWorkflowPluginFactory;
import com.atlassian.jira.plugin.workflow.WorkflowPluginConditionFactory;
import com.opensymphony.workflow.loader.AbstractDescriptor;

public class HasAttachmentsConditionFactory extends AbstractWorkflowPluginFactory implements WorkflowPluginConditionFactory {
    @Override
    public Map<String, ?> getDescriptorParams(Map<String, Object> params) {
        return new HashMap<String, Object>();
    }

    @Override
    protected void getVelocityParamsForEdit(Map<String, Object> velocityParams, AbstractDescriptor descriptor) {
    }

    @Override
    protected void getVelocityParamsForInput(Map<String, Object> velocityParams) {
    }

    @Override
    protected void getVelocityParamsForView(Map<String, Object> velocityParams, AbstractDescriptor descriptor) {
    }
}
